const
  fs = () => { return require('fs-extra'); },
  exec = require('child_process').exec,
  glob = () => { return require('glob'); },
  gulp = require('gulp'),
  jsdom = require('jsdom'),
  source = () => { return require('vinyl-source-stream'); },
  serialize = () => { return require('w3c-xmlserializer'); },
  browserify = () => { return require('browserify'); };

function cli(command, options) {
  return new Promise((res, rej) => {
    options = options || {};
    options['maxBuffer'] = 1024 * 500;
    let proc = exec(command, options, (err) => {
      if (err) 
        rej();
      else
        res();
    });
    proc.stdout.pipe(process.stdout);
    proc.stderr.pipe(process.stderr);
  });
}

async function iterateGlob(pattern, delegate) {
  return new Promise((res) => {
    glob()(pattern, async (err, entries) => {
      for (let i = 0; i < entries.length; i++) {
        await delegate(entries[i]);
      }
      res();
    });
  });
}

async function copy(from, to) {
  if (Array.isArray(from)) {
    for (let i = 0; i < from.length; i++) {
      await copy(from[i], to);
    }
  }
  else {
    let exists = fs().existsSync(from);
    if (exists) {
      let stat = await fs().promises.stat(from);
      if (stat.isFile()) {
        let filename = from.substring(from.lastIndexOf('/') + 1);
        if (!to.endsWith(filename)) {
          to += `/${filename}`;
        }
      }
      return fs().copy(from, to);
    }
    else {
      throw `${from} does not exist.`;
    }
  }
}
async function copyPattern(fromPattern, toDir) {
  // return new Promise((res) => {
  //   glob()(fromPattern, async (err, entries) => {
  //     for (let i = 0; i < entries.length; i++) {
  //       await copy(entries[i], toDir);
  //     }
  //     res();
  //   });
  // });
  await iterateGlob(fromPattern, async (entry) => {
    await copy(entry, toDir);
  });
}

async function remove(path) {
  await fs().remove(path);
}
async function removePattern(pattern) {
  await iterateGlob(pattern, async (entry) => {
    await remove(entry);
  });
}

async function readFile(path) {
  return (await fs().promises.readFile(path)).toString();
}

async function loadXmlDocument(path) {
  let xml = await readFile(path);
  let dom = new jsdom().JSDOM(xml, {
    contentType: 'text/xml'
  });
  return dom.window.document;
}

async function writeFile(path, data) {
  await fs().promises.writeFile(path, data);
}

async function writeXmlDocument(document, path) {
  let xml = serialize()(document);
  xml = '<?xml version="1.0" encoding="UTF-8"?>\r\n' + xml;
  await writeFile(path, xml);
}

async function transpile(includePattern, outFilename, destPath) {
  return new Promise((res) => {
    glob()(includePattern, (err, entries) => {
      browserify()({ entries })
        .bundle()
        .pipe(source()(outFilename))
        .pipe(gulp.dest(destPath))
        .on('end', res);
    });
  });
}

async function createDirectory(path) {
  await fs().promises.mkdir(path);
}

module.exports = {
  cli,
  copy,
  remove,
  readFile,
  transpile,
  writeFile,
  copyPattern,
  removePattern,
  createDirectory,
  loadXmlDocument,
  writeXmlDocument
};